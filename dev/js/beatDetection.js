var BeatDetection = function(obj)
{
	this.sc = obj;

	this.detectLife 	= [0,0,0,0,0,0,0,0];
	this.detectComplete = [0,0,0,0,0,0,0,0];
	this.detectThreshold = [0.8,0.3,0.4,0.5,0.5,0.5,0.5,0.5];
	this.detectThresholdBounds = [
		[0.6,0.1,0.2,0.5,0.5,0.5,0.5,0.5],
		[0.9,0.5,0.6,0.5,0.5,0.5,0.5,0.5]
	];

	this.moduloDetect = [0,0,0,0,0,0,0,0];
	this.moduloDetectBounds = [3,2,1,0,0,0,0,0];
};

BeatDetection.prototype.detect = function(event)
{

	if(event.count % 120 == 0)
	{
		for(var i = 0; i < this.moduloDetect.length; i++)
		{
			if(this.moduloDetect[i] == 0)
			{
				this.detectThreshold[i] -= 0.02;

				if(this.detectThreshold[i]<this.detectThresholdBounds[0][i])
				{
					this.detectThreshold[i] = this.detectThresholdBounds[0][i];
				}
			}
			else if(this.moduloDetect[i] >= this.moduloDetectBounds[i])
			{
				this.detectThreshold[i] += 0.06;

				if(this.detectThreshold[i]>this.detectThresholdBounds[1][i])
				{
					this.detectThreshold[i] = this.detectThresholdBounds[1][i];
				}
			}
		}
		this.moduloDetect = [0,0,0,0,0,0,0,0];
	}
	for(var i = 0; i < this.sc.app.fft.length; i++)
	{
		if(this.sc.app.fft[i]>this.detectThreshold[i])
		{
			if(this.detectLife[i] == 0)
			{
				// console.log("DETECT "+i);
				this.detectLife[i] = 10 * (i+1);

				if(i == 0)
				{
					this.detectLife[i] = 1;
				}
				this.detectComplete[i] += 1;
				this.moduloDetect[i] += 1;

				var bottom = this.sc.app.fft[i] - this.detectThreshold[i],
					top = 1 - this.detectThreshold[i];

				var perc = bottom / top;

				switch(i)
				{
					case 0:
						this.sc.app.stars.randomLarge();
						break;
					case 1:
						this.sc.app.land.addMarker(perc);
						break;
					case 2:

						this.sc.app.land.addMountain(perc);
						break;
					default:
						console.log(i);
						break;
				}
			}
			else
			{
				// console.log("DETECTED BUT NAH "+i);
				this.detectLife[i] -= 1;
			}
		}
	}
};