var Land = function(obj)
{
	this.app = obj;

	var bounds = this.app.square.bounds;

	this.land = new Path([
        new Point(bounds.x,bounds.y + (bounds.height * 0.85)),
        new Point(bounds.x + (bounds.width * 0.5),bounds.y + (bounds.height * 0.8)),
        new Point(bounds.x + bounds.width,bounds.y + (bounds.height * 0.85)),
    	new Point(bounds.x + bounds.width,bounds.y + bounds.height + 2),
    	new Point(bounds.x,bounds.y + bounds.height + 2),
    	new Point(bounds.x,bounds.y + (bounds.height * 0.85))
    ])
	this.land.fillColor = this.app.currColour[0];
	// this.land.selected = true;

	this.land.segments[1].handleIn = new Point((bounds.width/4)*-1,0);
	this.land.segments[1].handleOut = new Point((bounds.width/4),0);

	this.animatePath = new Path([
        new Point(bounds.x,bounds.y + (bounds.height * 0.85)),
        new Point(bounds.x + (bounds.width * 0.5),bounds.y + (bounds.height * 0.8)),
        new Point(bounds.x + bounds.width,bounds.y + (bounds.height * 0.85))
	]);
	// this.animatePath.selected = true;

	var len = this.animatePath.length,
		start = len * 0.11,
		end = len * 0.89;

	this.animatePath.segments[1].handleIn = new Point((bounds.width/4)*-1,0);
	this.animatePath.segments[1].handleOut = new Point((bounds.width/4),0);

	// var startP = this.animatePath.getLocationAt(start),
	// 	endP = this.animatePath.getLocationAt(end);

	// this.animatePath.segments[0].point = startP.point;
	// this.animatePath.segments[2].point = endP.point;

	this.mountains = [];
	this.mountainGroup = new Group();

	project.layers[0].addChild(this.mountainGroup);

	this.markers = [];
	this.markerGroup = new Group();

	project.layers[0].addChild(this.markerGroup);
};

Land.prototype.addMountain = function(perc)
{
	this.mountains.push(new MountainRange(this,perc));
}

Land.prototype.addMarker = function(perc)
{
	this.markers.push(new Marker(this,perc));
};

Land.prototype.update = function()
{
	for(var i = 0; i < this.mountains.length; i++)
	{
		this.mountains[i].update();
	}
};

Land.prototype.onFrame = function(event)
{
	this.update();

	var counter = 0;
	for(var i = 0; i < this.markers.length; i++)
	{
		if(this.markers[counter].destroy)
		{
			this.markers.splice(counter,1);
		}
		else
		{
			this.markers[counter].draw();
			counter++;
		}
	}
};

Land.prototype.resize = function()
{
	var bounds = this.app.square.bounds;

	this.land.position.x = this.app.square.position.x;
	this.land.position.y = (bounds.y + bounds.height) - (this.land.bounds.height/2);

	this.animatePath.position = new Point(bounds.x + (bounds.width * 0.5),(bounds.y + (bounds.height * 0.8)) + (this.animatePath.bounds.height / 2))
}


