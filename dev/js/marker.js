var Marker = function(obj,perc)
{
	this.landParent = obj;

	this.destroy = false;
	this.perc = 0;

	var point = this.getPoint();

	var hei = (this.landParent.app.square.bounds.height * 0.1),
		hei = (hei) + (hei * perc);

	var toPoint = point.clone();
	toPoint.y = parseFloat(toPoint.y) - hei;

	this.shape = new Path.Circle(point,2);
	this.shape.visible = false;
	
	this.angleExtremes = 12;

	this.shape.rotate(this.angleExtremes);
	//this.shape.selected = true;

	this.actShape = new Path.Line(point,toPoint);
	this.actShape.rotate(this.angleExtremes);
	this.actShape.visible = false;

	this.tree = new Path();
	var p1 = new Point(point.x - 10,point.y);
	this.tree.add(p1);

	var treePoints = 1;
	for(var i = 0; i < 8; i++)
	{
		if(Math.random()>0.6)
		{
			var c = this.landParent.app.pointLerp(p1,toPoint,i/10);

			this.tree.add(new Point(c.x + 1,c.y-1));
			this.tree.add(new Point(c.x - Math.random() * 2,c.y));
			treePoints += 2;
		}
	}

	this.tree.add(toPoint);
	var p2 = new Point(point.x + 10,point.y);


	for(var i = 4; i < 10; i++)
	{
		if(Math.random()>0.6)
		{
			var c = this.landParent.app.pointLerp(toPoint,p2,i/10);

			this.tree.add(new Point(c.x + 1,c.y-1));
			this.tree.add(new Point(c.x - Math.random() * 2,c.y));
		}
	}

	this.tree.add(p2);
	this.tree.fillColor = this.landParent.app.currColour[0];
	this.tree.closed = true;
	//this.tree.selected = true;


	this.treeGrad = this.tree.clone();

	for(var i = (treePoints+1); i < this.tree.segments.length; i = i+2)
	{
		if((i+1) == this.tree.segments.length)
		{
			this.treeGrad.segments[i].point.x = this.tree.bounds.x + (this.tree.bounds.width / 2);
		}
		else
		{
			this.treeGrad.segments[i].point.x = (this.tree.bounds.x + (this.tree.bounds.width / 2)) + ((Math.random() * 2) - 1);
			this.treeGrad.segments[i+1].point.x = this.tree.bounds.x + (this.tree.bounds.width / 2);
		}
	}

	this.treeGrad.fillColor = {
		gradient: {
	        stops: [[new Color(255,255,255,0.45), 0.25], [new Color(255,255,255,0.25), 0.45], [new Color(100,100,100,0.05), 0.7]]
	    },
	    origin: this.tree.bounds.leftCenter,
	    destination: this.tree.bounds.bottomRight
	}
	this.treeGrad.translate(new Point(0,-1));

	this.tree.rotate(this.angleExtremes);
	this.treeGrad.rotate(this.angleExtremes);


	this.group = new Group();
	this.group.addChild(this.actShape);
	this.group.addChild(this.tree);
	this.group.addChild(this.treeGrad);

	this.landParent.markerGroup.addChild(this.group);
};

Marker.prototype.getPoint = function()
{
	var offset = this.landParent.animatePath.length -  (this.landParent.animatePath.length * this.perc);

	var p = this.landParent.animatePath.getPointAt(offset);
	p.y += 3;

	return p;
}

Marker.prototype.update = function()
{
	var mod = 0.002;
	this.perc += mod;

	this.shape.rotate(0 - ((this.angleExtremes*2) * mod));

	if(this.perc>=1)
	{
		this.destroy = true;
		this.shape.remove();
		this.group.remove();
	}
	else
	{
		this.tree.fillColor = this.landParent.app.currColour[0];
		this.shape.position = this.getPoint();
		this.group.position = this.shape.position;
		this.group.translate(0, 0 - this.group.bounds.height/2);
		this.group.rotate(0 - ((this.angleExtremes*2) * mod),this.shape.position);
	}
};

Marker.prototype.draw = function()
{
	this.update();
};

