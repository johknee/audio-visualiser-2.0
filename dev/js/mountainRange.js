var MountainRange = function(obj,perc)
{
	this.land = obj;
	this.perc = perc;

	var bounds = this.land.app.square.bounds;

	this.mountainGroup = new Group();

	var rand = 0.3 + (0.4 * perc);

	var mainPoints = [
		new Point((bounds.x + (bounds.width / 1.8)) - (bounds.width * 0.4), (bounds.y + bounds.height)),
		new Point((bounds.x + (bounds.width / 1.8)), (bounds.y + bounds.height) - (bounds.width * rand)),
		new Point((bounds.x + (bounds.width / 1.8)) + (bounds.width * 0.4), (bounds.y + bounds.height))
	];

	this.mountain = new Path();
	this.mountain.add(mainPoints[0]);

	var frontFace = 1;

	for(var i = 0; i < 10; i++)
	{
		var rand = Math.random();

		if(rand>0.7)
		{
			var p = this.land.app.pointLerp(mainPoints[0],mainPoints[1],i/10);

			p.x += ((Math.random() * 2) - 1) * 10;
			p.y += ((Math.random() * 2) - 1) * 4;

			this.mountain.add(p);
			frontFace++;
		}
	}

	this.mountain.add(mainPoints[1]);

	for(var i = 3; i < 10; i++)
	{
		var rand = Math.random();

		if(rand>0.7)
		{
			var p = this.land.app.pointLerp(mainPoints[1],mainPoints[2],i/10);

			p.x += ((Math.random() * 2) - 1) * 10;
			p.y += ((Math.random() * 2) - 1) * 4;

			this.mountain.add(p);
		}
	}

	this.mountain.add(mainPoints[2]);
	this.mountain.closed = true;
	this.mountain.fillColor = this.land.app.currColour[0];
	this.mountain.translate(new Point((this.mountain.bounds.width * 0.85),0 - (this.mountain.bounds.height/6)));


	/*
	this.compound = new CompoundPath({
	    children: this.lines,
	    fillColor: this.land.app.currColour[0],
	    //selected:true
	});
	this.compound.translate(new Point((this.mountain.bounds.width * 0.85),0 - (this.mountain.bounds.height/6)));
	*/

	this.gradient = this.mountain.clone();

	this.gradient.fillColor = {
		gradient: {
	        stops: [[new Color(255,255,255,0.25), 0.05], [new Color(255,255,255,0.1), 0.15], [new Color(0,0,0,0.1), 0.7]]
	    },
	    origin: this.mountain.bounds.topCenter,
	    destination: this.mountain.bounds.bottomRight
	};

	this.frontFace = this.mountain.clone();

	var center = this.frontFace.segments[frontFace].point;

	for(var i = 0; i < frontFace; i++)
	{
		var diff = center.x - this.frontFace.segments[i].point.x;
		this.frontFace.segments[i].point.x += (diff/2) + ((diff/2)* Math.random());
	}
	//this.frontFace.selected = true;

	this.mountainGroup.addChild(this.mountain);
	this.mountainGroup.addChild(this.gradient);
	this.mountainGroup.addChild(this.frontFace);

	this.land.mountainGroup.addChild(this.mountainGroup);
};

MountainRange.prototype.update = function()
{
	this.mountain.fillColor = this.land.app.currColour[0];
	this.frontFace.fillColor = this.land.app.currColour[0];

	this.mountainGroup.position.x -= 0.5;
}