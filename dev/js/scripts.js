// ==============================================
// 
// Object
// 
// ==============================================

	var JTApp = function(prop) {
		$.extend(this,prop);

	    this.client = 'c9b678bd0a44ce855ba92eaefb390134';
	    this.debug = false;
	    this.playing = false;
	    this.restart = false;
	    this.restartCount = 0;

	    this.SC = SC;
		this.init();
	};

	JTApp.prototype.init = function() {
		// Attach paper js variables and handlers to work with
			paper.install(window);
			paper.setup('audioCanvas');
			view.onFrame = this.onFrame;
			view.onResize = this.onResize;

		this.trackSet = false;
		this.menu = true;

		if($("body").data("url")!="")
	    {
	    	this.trackSet = true;
	    	this.menu = false;
	    }

	    this.setupHistory();

	    this.setupDownload();

		this.setColours();

	    this.soundcloud = new SoundcloudObject(this);

	    this.canvasArea = this.getCenter();

	    this.createSkylines();

	    this.createBounds();

	    this.createStars();

		this.createMask();

	    this.createLand();

		this.fire();
		this.animationLooper();
	};

	JTApp.prototype.fire = function() {

		this.events();
	};

	JTApp.prototype.events = function() {
		var self = this;

		$(window).resize(this.setSize);

		$(window).keyup(function(e) {

			console.log(e.keyCode);

			switch(e.keyCode)
			{
				case 65:
					self.land.addMarker();
					break;
			}
		});

		$(".info_toggle").click(function(e) {
			e.preventDefault();

			self.toggleMenu();

			return false;
		});

		$(".input_wrap button").click(function(e) {
			e.preventDefault();
			self.soundcloud.autoplay = false;
			self.soundcloud.getTrack($("#soundcloud_url").val());
			return false;
		});
		$(".input_wrap input").keyup(function(e) {

			if(e.keyCode==13)
			{
				self.soundcloud.autoplay = false;
				self.soundcloud.getTrack($("#soundcloud_url").val());
			}
		});

		$(".about_toggle").click(function(e) {
			e.preventDefault();
			self.toggleAbout();
			return false;
		});


	};

	JTApp.prototype.animationLooper = function()
	{
		var self = this;

		var scrollT 	= $(window).scrollTop(),
			windH 		= $(window).height(),
			windW 		= $(window).width(),
			scrollB 	= scrollT + windH,
			docH 		= $(document).height();

		//requestAnimFrame(this.animationLooper.bind(this));
	};

	/*
	=======================

	DOM EVENTS ETC.

	=======================
	*/

		JTApp.prototype.toggleMenu = function()
		{
			var self = this;

			if(self.menu)
			{
				self.menu = false;
				$(".info_toggle").removeClass("close");
				$("body").removeClass("about-show");
				$("#soundcloud").fadeOut(500, function()
				{
					$(this).removeClass("show");

				});
			}
			else
			{
				self.menu = true;
				$(".info_toggle").addClass("close");
				$("#soundcloud").fadeIn(500, function()
				{
					$(this).css("display","table");

					setTimeout(function()
					{
						$("#soundcloud").addClass("show");
					},100);
				});
			}
		}

		JTApp.prototype.toggleAbout = function()
		{
			var self = this;

			$("body").toggleClass("about-show");
		}

		JTApp.prototype.setupHistory = function() {
			var self = this;

			this.history = window.History;

			this.history.Adapter.bind(window,'statechange',function(){
				var State = self.history.getState();

				if(State.data.permalink_url!=null)
				{
					/*
					If google analytics is setup, this can 
					push the ajax call to google analytics

					ga('send', 'pageview', {'page': State.data.slug,'title': State.data.title});
					*/


					//self.getContent(State.data);
				}
				else
				{
					//self.getContent(null);
				}
			});
		};

	/*
	=======================

	UTILITIES

	=======================
	*/

		JTApp.prototype.lerp = function(v1,v2,p)
		{
			return (1-p)*v1 + p*v2;
		}

		JTApp.prototype.colourLerp = function(c1,c2,p)
		{
			var copy1 = c1.clone(),
				copy2 = c2.clone();

			var rgb = {
				r : this.lerp(copy1.red,copy2.red,p),
				g : this.lerp(copy1.green,copy2.green,p),
				b : this.lerp(copy1.blue,copy2.blue,p)
			};

			return new Color(rgb.r,rgb.g,rgb.b);
		}

		JTApp.prototype.pointLerp = function(p1,p2,p)
		{
			var copy1 = p1.clone(),
				copy2 = p2.clone();

			var point = {
				x : this.lerp(copy1.x,copy2.x,p),
				y : this.lerp(copy1.y,copy2.y,p)
			};

			return new Point(point.x,point.y);
		}

		JTApp.prototype.pad = function(text)
		{
			var str = "" + String(text);
			var pad = "00";
			var ans = pad.substring(0, pad.length - str.length) + str;

			return ans;
		}

		JTApp.prototype.setupDownload = function()
		{
			var self = this;
			document.getElementById('download').addEventListener('click', self.download, false);
		};

		JTApp.prototype.download = function()
		{
			var dt = $("canvas").get(0).toDataURL();

			var str = "Audio Landscape - "+app.soundcloud.track.title+"_"+app.soundcloud.song.position + ".png";
			this.download = str.split(' ').join('_');
			this.href = dt;
		};

	/*
	=======================

	PAPER.JS HANDLERS

	=======================
	*/

		JTApp.prototype.onFrame = function(event)
		{
			if(app.playing)
			{
				for(var i = 0; i < app.skylines.length; i++)
				{
					app.skylines[i].onFrame(event);
				}

				app.land.onFrame(event);

				app.updateColour();

				app.soundcloud.beat.detect(event); // Do beat detection on every frame

				app.stars.onFrame(event);
			}
		};

		JTApp.prototype.onResize = function(event) {
			app.square.position = view.center;
			app.createMask();
			app.land.resize();

			for(var i = 0; i < app.skylines.length; i++)
			{
				app.skylines[i].resize();
			}
		}


	/*
	=======================

	CANVAS/BOUNDS FUNCTIONS

	=======================
	*/

		JTApp.prototype.setSize = function()
		{
			var desiredWidth = $(window).width();
			var desiredHeight = $(window).height();

			view.viewSize = new Size(desiredWidth, desiredHeight);
			view.draw();
		}

		JTApp.prototype.getCenter = function()
		{
			this.canvasSize = view.bounds.height * 0.6;

			var x = view.center.x - (this.canvasSize / 2),
				y = view.center.y - (this.canvasSize / 2);

			var points = {
				topLeft : new Point(x,y),
				bottomRight : new Point(x + this.canvasSize, y + this.canvasSize)
			}

			this.square = new Path.Rectangle(new Point(x,y),new Size(this.canvasSize,this.canvasSize));
			this.square.fillColor = this.currColour[1];

			return points;
		};


	/*
	=======================

	SETTING VARIABLES

	=======================
	*/

		JTApp.prototype.setColours = function()
		{
			this.colours = [
				[
					new Color(0.75, 0.89, 0.75),
					new Color(0.85, 0.91, 0.69),
					new Color(0.97, 0.91, 0.65),
					new Color(1, 0.84, 0.57),
					new Color(1, 0.74, 0.53)
				],
				[
					new Color(0.64, 0.98, 1),
					new Color(0.71, 0.93, 0.78),
					new Color(0.78, 0.9, 0.61),
					new Color(0.85, 0.86, 0.41),
					new Color(1, 0.78, 0)
				],
				[
					new Color(0.82, 0.59, 0.71),
					new Color(0.9, 0.64, 0.65),
					new Color(0.96, 0.71, 0.6),
					new Color(1, 0.75, 0.55),
					new Color(1, 0.83, 0.48)
				],
				[
					new Color(0.19, 0.22, 0.34),
					new Color(0.33, 0.34, 0.51),
					new Color(0.68, 0.53, 0.66),
					new Color(1, 0.65, 0.74),
					new Color(1, 0.86, 0.73)
				],
				[
					new Color(0, 0.24, 0.31),
					new Color(0.2, 0.22, 0.31),
					new Color(0.4, 0.19, 0.3),
					new Color(0.6, 0.17, 0.29),
					new Color(0.8, 0.15, 0.28)
				]
			];

			this.currColour = this.colours[0];
		};

		JTApp.prototype.updateColour = function()
		{
			var skipOver = false;

			if(this.restart)
			{
				var s = this.restartCount,
					e = 30,
					p = (view._count - this.restartCount) / e;
				var perc = p;

				if(perc>=1)
				{
					this.restart = false;
					this.soundcloud.playing = false;
					this.soundcloud.perc = 0;
					this.soundcloud.getTrack(this.soundcloud.restartTrack);
					skipOver = true;
				}
			}
			else
			{
				var perc = this.soundcloud.perc - 0.01;
			}

			if(!skipOver)
			{
				if(perc<0)
				{
					perc = 0;
				}

				var curr = Math.floor(perc * 4),
					targ = curr + 1;

				if(this.restart)
				{
					curr = Math.floor(this.restartPerc * 4);
					targ = 0;
				}

				var split = (1 / (this.colours.length-1)),
					cP = perc - (split * curr);

				if(targ==this.colours.length)
				{
					curr--;
					targ--;
					//cP = split;
				}

				var cAct = map_range(cP,0,split,0,1);

				if(this.restart)
				{
					cAct = perc;
				}

				var lerp = [];

				for(var i = 0; i < this.colours[0].length; i++)
				{
					var cur = this.colours[curr][i].clone(),
						tar = this.colours[targ][i].clone();

					lerp.push(this.colourLerp(cur,tar,cAct));
				}

				this.currColour = lerp;

				this.centerCircle.fillColor = this.currColour[0];
				this.square.fillColor = this.currColour[1];


				this.land.land.fillColor = this.currColour[0];

				//$("#about p,#about h2").css("color",this.currColour[4].toCSS(true));
				//$("#about").css("background-color",this.currColour[4].toCSS(true));
				$("#about .bottom").css("background-color",this.currColour[4].toCSS(true));
			}
		};


	/*
	=======================

	CREATE/INITIATE FUNCTIONS

	=======================
	*/

		// Using the center variables this creates the bounding box the size of the circle mask

		JTApp.prototype.createBounds = function()
		{
			var rect = new Rectangle(this.canvasArea.topLeft, this.canvasArea.bottomRight);
			this.boundPath = new Path.Rectangle(rect);

		    if(this.debug)
		    {
				this.boundPath.strokeColor = 'red';
			}
		}

		// This is the mask that creates the circle in the center

		JTApp.prototype.createMask = function()
		{
			if(typeof this.centerCircle !== 'undefined')
			{
				this.centerCircle.remove();
				this.centerCircle = new CompoundPath({
				    children: [
				        new Path.Rectangle(new Point(0,0),new Size(view.size.width,view.size.height)),
				        new Path.Circle({
				            center: new Point(view.center.x,view.center.y),
				            radius: this.canvasSize / 2
				        })
				    ],
				    fillColor: this.currColour[0],
				    // selected: true
				});
			}
			else
			{
				var secondLayer = new Layer();
				this.centerCircle = new CompoundPath({
				    children: [
				        new Path.Rectangle(new Point(0,0),new Size(view.size.width,view.size.height)),
				        new Path.Circle({
				            center: new Point(view.center.x,view.center.y),
				            radius: this.canvasSize / 2
				        })
				    ],
				    fillColor: this.currColour[0],
				    // selected: true
				});

				project.activeLayer = project.layers[0];
			}
		};

		// These are the waves in the sky

		JTApp.prototype.createSkylines = function()
		{
		    this.skylines = [];

		    for(var i = 0; i < 3; i++)
		    {
			    this.skylines.push(new SkyLine(this,i));
			    this.skylines[i].setBounds(this.canvasArea);
			    this.skylines[i].update();
		    }
		};

		// This is the bottom of the circle, where the elements sit on

		JTApp.prototype.createLand = function()
		{
			this.land = new Land(this);
		};

		// These are the lovely twinkly stars in the sky

		JTApp.prototype.createStars = function()
		{
			this.stars = new Stars(this);
		};







// ==============================================
// 
// Initiate
// 
// ==============================================

	var app;

	$(document).ready(function() {
		app = new JTApp({
			site : $("body").data("site")
		});

	});







