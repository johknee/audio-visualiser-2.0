var SkyLine = function(obj,index)
{
	this.app = obj;
	this.index = index;

	this.max = 150;
	this.start = this.max * -1;
	this.divisions = 50;

	this.points = [];

	for(var i = 0; i < (this.max / this.divisions) * 2; i++)
	{
		this.points.push({
			perc_x : this.start,
			perc_y : 0
		});

		this.start += this.divisions;
	}

	var temp = [];

	for(var i = 0; i < this.points.length; i++)
	{
		temp.push(new Point(0,0));
	}

	this.path = new Path(temp);
	this.path.fillColor = this.app.currColour[this.index+2];
	// this.path.selected = true;
};

SkyLine.prototype.setBounds = function(obj) {
	this.rect = new Rectangle(obj.topLeft, obj.bottomRight);

	var hei = this.rect.height;

	this.rect.height = hei * 0.2;
	//this.rect.y = (this.rect.y + ((hei * 0.8) / 2)) + ((this.app.canvasSize * 0.2) * this.index);
	this.rect.y = this.rect.y + ((this.app.canvasSize * 0.2) * (this.index + 0.5));

	var bounds = this.app.square.bounds;

	this.path.add(new Point(bounds.x + (bounds.width * 2),this.rect.y + (this.rect.height * 0.5)));
	this.path.add(new Point(bounds.x + (bounds.width * 2),bounds.y + (bounds.height*4)));
	this.path.add(new Point(0,bounds.y + (bounds.height*4)));
	this.path.add(new Point(0,this.rect.y + (this.rect.height * 0.5)));

	this.boundPath = new Path.Rectangle(this.rect);
	// this.boundPath.selected = true;

	if(this.app.debug)
	{
		this.boundPath.strokeColor = 'green';
	}
};

SkyLine.prototype.generatePathPoints = function() {

	var bounds = this.boundPath.bounds;

	this.pathPoints = [];

	for(var i = 0; i < this.points.length; i++)
	{
		var x = bounds.x + (bounds.width * ((this.points[i].perc_x)/100)),
			y = bounds.y + (bounds.height * ((this.points[i].perc_y)/100));

		this.pathPoints.push(new Point(x,y));
		this.path.segments[i].point.x = x;
		this.path.segments[i].point.y = y;
		var div = (this.app.square.bounds.width / this.divisions) / 2;
		this.path.segments[i].handleIn = new Point((this.app.square.bounds.width / div) * -1,0);
		this.path.segments[i].handleOut = new Point(this.app.square.bounds.width / div,0);
	}

	this.path.closed = true;
	//this.path.smooth();
};

SkyLine.prototype.update = function(event) {

	for(var i = 0; i < this.points.length; i++)
	{
		this.points[i].perc_x -= 0.5;

		if(i == (this.points.length - 1))
		{
			if(this.points[i].perc_x<=this.max - this.divisions)
			{
				this.points.splice(0,1);

				this.points.push({
					perc_x : this.max,
					perc_y : this.app.fftAvg[this.index] * 200
				});
			}
		}
	}

	this.generatePathPoints();
	this.path.fillColor = this.app.currColour[this.index+2];
};

SkyLine.prototype.draw = function(event) {

};

SkyLine.prototype.onFrame = function(event) {
	this.update(event);
	this.draw(event);
};

SkyLine.prototype.resize = function(obj) {
	this.boundPath.position.y = (view.center.y - (this.app.canvasSize / 2)) + ((this.app.canvasSize * 0.2) * (this.index + 1));
	this.boundPath.position.x = view.center.x;
};

