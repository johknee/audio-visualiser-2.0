var SoundcloudObject = function(obj)
{
	this.app = obj;
	this.SC = this.app.SC;
	this.track = null;
	this.url = null;
	this.app.fft = [0,0,0,0,0,0,0,0];
	this.app.fftAvg = [0,0,0,0,0,0,0,0];
	this.perc = 0;
	this.playlist = false;

    this.SC.initialize({
        client_id: this.app.client
    });

    if(this.app.trackSet)
    {
    	this.getTrack($("body").data("url"));
    }

    this.beat = new BeatDetection(this);
}

SoundcloudObject.prototype.regexURL = function(url){
	var regexp = /^https?:\/\/(soundcloud.com|snd.sc)\/(.*)$/;
	return url.match(regexp) && url.match(regexp)[2]
}

SoundcloudObject.prototype.getTrack = function(url)
{
	var self = this;

	if(this.regexURL(url))
	{
		this.url = url;

		if(this.track)
		{
			this.song.destruct();
			this.track = null;
			this.app.playing = false;
		}

		if(this.app.menu)
		{
			this.app.toggleMenu();
		}

		if(this.perc!=0)
		{
			if(this.playlist)
			{
				this.perc = 0;
				this.song.destruct();
				this.track = null;
				this.getTrack(url);
			}
			else
			{
				if(this.track)
				{
					this.song.destruct();
					this.track = null;
				}
				this.app.playing = true;
				this.app.restart = true;
				this.app.restartCount = view._count;
				this.app.restartPerc = this.perc;
				this.restartTrack = url;
				this.app.fft = [0,0,0,0,0,0,0,0];
				this.app.fftAvg = [0,0,0,0,0,0,0,0];
			}
		}
		else
		{
			$.getJSON("http://api.soundcloud.com/resolve.json",{ url : this.url, client_id : this.app.client }, function(info) {

				if(!self.autoplay)
				{
					if(typeof info.track_count !== "undefined")
					{
						self.playlist = true;
						self.playlistTracks = info.tracks;
						self.playlistIndex = 0;
						self.playlistObject = info;
						$("h5.playlist").show();

						$("h5.playlist").text(self.app.pad(self.playlistIndex+1)+"/"+self.app.pad(self.playlistTracks.length));
					}
					else
					{
						self.playlist = false;
						$("h5.playlist").hide();
					}
				}

				if(self.playlist)
				{
					self.track = self.playlistTracks[self.playlistIndex];
					var obj = {
						permalink : self.app.site + self.playlistObject.user.permalink + "/sets/" + self.playlistObject.permalink,
						permalink_url : self.playlistObject.permalink_url
					};
				}
				else
				{
					self.track = info;
					var obj = {
						permalink : self.app.site + self.track.user.permalink + "/" + self.track.permalink,
						permalink_url : self.track.permalink_url
					};
				}

				var lnks = "";

				if(self.track.permalink_url)
				{
					lnks += '<li><a href="'+self.track.peramlink_url+'" target="_blank">Permalink</a></li>';
				}
				if(self.track.purchase_url)
				{
					lnks += '<li><a href="'+self.track.purchase_url+'" target="_blank">Purchase</a></li>';
				}

				var actTitle =  self.track.title + "\u2014" + "Audio Landscape";

				$("title").text(actTitle);

				self.app.history.pushState(obj, actTitle, obj.permalink);

				$("#soundcloud_url").val("");

				$("h2.track").text(self.track.title);
				$("h2.artist").text(self.track.user.username);

				$("#about .bottom").fadeIn(250);
				$("#about h4.title").text(self.track.title);
				$("#about h4.artist").text(self.track.user.username);
				$("#about .links").html(lnks);
				$("#about .image img").attr("src",self.track.artwork_url);
				self.streamTrack();
			});
		}
	}
	else
	{
		alert("This unfortunately isn't a valid soundcloud url");
	}
};

SoundcloudObject.prototype.streamTrack = function()
{
	var self = this;

	if(this.track)
	{
	    var opts = {
	    	useWaveformData : true,
	    	useEQData : true,
	    	onplay : function()
	    	{
	    		self.app.playing = true;
	    	},
	    	onpause : function()
	    	{
	    		self.app.playing = false;
	    	},
	    	onfinish : function()
	    	{
	    		self.app.playing = false;

	    		if(self.playlist)
	    		{
	    			self.playlistIndex++;
	    			self.autoplay = true;
					$("h5.playlist").text(self.app.pad(self.playlistIndex+1)+"/"+self.app.pad(self.playlistTracks.length));
	    			self.getTrack(self.playlistTracks[self.playlistIndex].permalink_url);
	    		}
	    		else
	    		{
		    		if(!self.app.menu)
		    		{
			    		self.app.toggleMenu();
		    		}
	    		}
	    	},
	    	onstop : function()
	    	{
	    		self.app.playing = false;
	    	},
	    	whileplaying : function()
	    	{
	    		var avgParts = [],
	    			parts = [],
	    			splitter = 8,
	    			waveData = this.eqData.left.slice(0),
	    			portion = this.eqData.left.length/splitter;

	    		for(var i = 0; i < splitter; i++)
	    		{
	    			var curr = waveData.splice(0,portion);

	    			var sum = curr.reduce(function(a, b) { return parseFloat(a) + parseFloat(b); });
					var avg = sum / curr.length;

		    		avgParts.push(avg);
		    		
		    		parts.push(parseFloat(this.eqData.left[portion * i]));
	    		}

	    		self.app.fftAvg = avgParts;
	    		self.app.fft = parts;

	    		self.perc = this.position / this.duration;
	    		$(".progress").width((self.perc * 100) + "%");
	    	}
	    }

	    this.SC.stream("/tracks/"+this.track.id, opts,function(stream){
	    	self.song = stream;
	    	self.song.play();
	    });
	}
	else
	{
		console.log("No track defined");
	}
};