var Stars = function(obj)
{
	this.app = obj;
	this.stars = [];

	for(var i = 0; i < 100; i++)
	{
		var x = this.app.square.bounds.x + (Math.random() * this.app.square.bounds.width),
			y = this.app.square.bounds.y + (Math.random() * (this.app.square.bounds.width/1.5));

		var amt = 5;
		var type = Math.floor(Math.random() * amt);

		var mod = 1 / amt;

		var curr = new Path.Circle(new Point(x,y),(type * mod)+mod);
		curr.type = type;
		curr.maxOpac = 1 - (type * 0.05);
		curr.fillColor = new Color(255,255,255,0);
		curr.scaleProp = 1;

		this.stars.push(curr);
	}
};

Stars.prototype.randomLarge = function()
{
	var ind = Math.floor(Math.random() * this.stars.length);

	this.stars[ind].scale(3);
	this.stars[ind].scaleProp = 3;
};

Stars.prototype.onFrame = function(event)
{
	for(var i = 0; i < this.stars.length; i++)
	{
		var curr = this.stars[i];

		curr.position.x -= 0.1 * curr.type;

		if(curr.position.x < this.app.square.bounds.x)
		{
			curr.position.x = this.app.square.bounds.x + this.app.square.bounds.width;
			curr.position.y = this.app.square.bounds.y + (Math.random() * (this.app.square.bounds.width/1.5));
		}
		if(curr.scaleProp > 1)
		{
			curr.scale(0.9);
			curr.scaleProp *= 0.9;
		}

		var percOpac = this.app.lerp(0,curr.maxOpac,this.app.soundcloud.perc);
		// var percOpac = 1;

		curr.fillColor = new Color(255,255,255,percOpac);
	}
};