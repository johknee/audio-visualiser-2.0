var gulp = require('gulp'),
    gulpLoadPlugins = require('gulp-load-plugins'),
    plugins = gulpLoadPlugins(),
	neat = require('node-neat').includePaths;
	
var paths = {
		js : {
			origin 	: "dev/js/*.js",
			dest	: "assets",
			path	: "dev/js"
		},
		css : {
			origin 	: "dev/**/*.scss",
			dest	: "assets",
			path	: "dev/css"
		},
		images : {
			origin 	: "dev/images/**",
			dest	: "assets/images",
			path	: "dev/images"
		}
	}

// GULP TASKS	
// ==========================================================

	gulp.task('js', function () {

		gulp.src([paths.js.origin,'!' + paths.js.path + '/keep{,/*.js}',])
			.pipe(plugins.plumber())
			.pipe(plugins.sourcemaps.init())
			.pipe(plugins.uglify())
			.pipe(plugins.concat('scripts.js'))
			.pipe(plugins.sourcemaps.write('.'))
			.pipe(gulp.dest(paths.js.dest+'/js'))
			.pipe(plugins.livereload());

	});

	gulp.task('keepjs', function() {


		gulp.src([paths.js.path + '/keep/*.js'])
			.pipe(plugins.plumber())
			.pipe(plugins.sourcemaps.init())
			.pipe(plugins.uglify())
			.pipe(plugins.sourcemaps.write('.'))
			.pipe(gulp.dest(paths.js.dest+'/js'))
			.pipe(plugins.livereload());
	});
	
	gulp.task('css', function() {
		gulp.src(paths.css.path+"/font/*", { base: './dev/css' })
			.pipe(gulp.dest(paths.css.dest+'/css/'));
		
		gulp.src(paths.css.origin)
			.pipe(plugins.plumber())
			.pipe(plugins.sourcemaps.init())
			.pipe(plugins.sass({
				outputStyle: 'compressed',
				includePaths : [paths.css.path].concat(neat)
			}))
			.pipe(plugins.sourcemaps.write())
			.pipe(gulp.dest(paths.css.dest))
			.pipe(plugins.livereload());

	});
	
	gulp.task('images', function() {
		gulp.src(paths.images.path+"/*", { base: './dev/images' })
			.pipe(gulp.dest(paths.images.dest));

	});

// WATCH FILES
// ==========================================================
// run: gulp

	gulp.task('default', function () {
		plugins.livereload.listen();

		gulp.run('keepjs');
		gulp.run('js');
		gulp.watch(paths.js.origin, ['js']);
		
		gulp.run('css');
		gulp.watch(paths.css.origin, ['css']);
		
		gulp.run('images');
		plugins.watch(paths.images.origin, function (files) {
			gulp.start('images');
		});

	});


// CREATE DISTRIBUTION FILES
// ==========================================================
// Run: gulp dist

	gulp.task('dist',function() {

		gulp.src(["**/*",
			"!.gitkeep",
			"!.gitignore",
			"!composer.json",
			"!gulpfile.js",
			"!package.json",
			'!{dev,dev/**}',
			'!{build,build/**}',
			'!{node_modules,node_modules/**}',
			'!README.md',
			'!CommonCode.php'])
			.pipe(gulp.dest('build/'));
	});



