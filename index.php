<?php 

require_once "vendor/autoload.php";

$url = 'https://soundcloud.com/';

$app = new \Slim\Slim();
$app->get('/', function () use ($app) {
    //$url = 'https://soundcloud.com/tycho/tycho-awake';
    $url = '';
    $app->render('home.php', array('url' => $url));
});
$app->get('/:artist/:track', function ($artist,$track) use ($app) {
    $url = 'https://soundcloud.com/'.$artist.'/'.$track;
    $app->render('home.php', array('url' => $url));
});
$app->get('/:artist/sets/:playlist', function ($artist,$playlist) use ($app) {
    $url = 'https://soundcloud.com/'.$artist.'/sets/'.$playlist;
    $app->render('home.php', array('url' => $url));
});
$app->run();
?>
