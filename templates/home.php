<?php
$site = "http://localhost:8888/audionature/";
?>
<!DOCTYPE html>
<html>
<head>
    <!-- - - META TAGS - - -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Audio Landscape</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- - - END META TAGS - - -->

    <!-- - - SCRIPTS - - -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="//connect.soundcloud.com/sdk.js"></script>
    <script src="//use.typekit.net/rcx4eeg.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <script src="<?php echo $site;?>assets/js/paper.js"></script>
    <script src="<?php echo $site;?>assets/js/scripts.js"></script>
    <!-- - - END SCRIPTS - - -->
    
    
    <!-- - - STYLESHEETS - - -->
    <link rel="stylesheet" href="<?php echo $site;?>assets/css/styles.css">
    <!-- - - END STYLESHEETS - - -->
</head>

<body data-url="<?php echo $url;?>" data-site="<?php echo $site;?>">

    <!--[if lte IE 8]>
    <div id="ie-overlay">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td>
                    <h2>Your browser is outdated</h2>
                    <p>
                        Unfortunately the browser you are using is outdated and you are unable to view our site with it. For a better internet experience please update to the latest version, or even try <a href="https://www.google.com/chrome/browser/desktop/index.html">Google Chrome</a> or <a href="https://www.mozilla.org/en-GB/firefox/new/">Mozilla Firefox</a>
                    </p>
                </td>
            </tr>
        </table>
    </div>
    <![endif]-->


    <a href="#" id="download" download="AudioLandscape.png">Download</a>

    <a href="#" class="info_toggle <?php if($url=="") echo 'close';?>"></a>

    <div id="about">
        <div class="top">
            <a href="#" class="about_toggle">Close</a> 
        </div>
        <div class="middle">
            <h2>Music is a journey.</h2>
            <p>
                Audio Landscape is a music visualiser, that generates a world for you from your favourite songs. I wanted to create something that gave light to the moods that music can take you through.
            </p>
            <p>
                The site was designed and developed by <a href="http://jthaw.me" target="_blank">Jonny Thaw</a>. The site is powered Soundcloud's API and built using paper.js.
            </p>
        </div>
        <div class="bottom">
            <div class="image">
                <img src=""/>
            </div>
            <div class="info">
                <div class="cont">
                    <h4 class="title"></h4>
                    <h4 class="artist"></h4>
                    <ul class="links">
                        <li><a href="#">Shop</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="soundcloud" <?php if($url!="") echo 'style="display:none;"';?>>
        <div class="progress"></div>
        <div class="top">
            <a href="#" class="about_toggle">Info</a>
        </div>
        <div class="middle">
            <h5 class="playlist">01/06</h5>
            <h2 class="track"></h2>
            <h2 class="artist"></h2>
        </div>
        <div class="bottom">

            <div class="input_wrap">
                <button>Play</button>
                <input type="text" id="soundcloud_url" placeholder="Soundcloud URL"/>
            </div>
        </div>
    </div>

    <canvas id="audioCanvas" resize keepalive="true"></canvas>

</body>
</html>